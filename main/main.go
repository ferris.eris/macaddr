package main

import (
	"net"
	"net/http"

	"github.com/labstack/echo"
)

func getMacAddr() ([]string, error) {
	ifas, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	var as []string
	for _, ifa := range ifas {
		a := ifa.HardwareAddr.String()
		if a != "" {
			as = append(as, a)
		}
	}
	return as, nil
}

func main() {
	var result string
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		as, err := getMacAddr()
		if err != nil {
			result += err.Error()
		} else {
			for _, a := range as {
				result += a
				result += "\n"
			}
			result += "\n"
		}
		return c.String(http.StatusOK, result)
	})
	e.Logger.Fatal(e.Start(":5000"))
}
